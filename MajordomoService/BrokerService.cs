﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZeroMQ;
using Majordomolib;

namespace MajordomoService
{
    public partial class BrokerService : ServiceBase
    {
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _brokerThread;
        System.Diagnostics.EventLog eventLog1;
        public BrokerService()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("BrokerSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "BrokerSource", "BrokerNewLog");
            }
            eventLog1.Source = "BrokerSource";
            eventLog1.Log = "BrokerNewLog";
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("In OnStart");
            _brokerThread = new Thread(brokerFunction);
            _brokerThread.Name = "My Worker Thread";
            _brokerThread.IsBackground = true;
            _brokerThread.Start();
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In OnStop.");
            _shutdownEvent.Set();
            if (!_brokerThread.Join(3000))
            { // give the thread 3 seconds to stop
                _brokerThread.Abort();
            }
        }

        private void brokerFunction()
        {
            eventLog1.WriteEntry("In Broker function.");
            bool Verbose = false;
            CancellationTokenSource cancellor = new CancellationTokenSource();
            Console.CancelKeyPress += (s, ea) =>
            {
                ea.Cancel = true;
                cancellor.Cancel();
            };
            eventLog1.WriteEntry("before using");
            using (Majordomolib.Broker broker = new Majordomolib.Broker(Verbose))
            {
                eventLog1.WriteEntry("after using");
                broker.Bind(MajordomoService.Properties.Settings1.Default.brokerIP);
                // Get and process messages forever or until interrupted
                while (!_shutdownEvent.WaitOne(0))
                {
                    if (cancellor.IsCancellationRequested)
                        broker.ShutdownContext();

                    var p = ZPollItem.CreateReceiver();
                    ZMessage msg;
                    ZError error;
                    if (broker.Socket.PollIn(p, out msg, out error, Majordomolib.MdpCommon.HEARTBEAT_INTERVAL))
                    {
                        using (ZFrame sender = msg.Pop())
                        using (ZFrame empty = msg.Pop())
                        using (ZFrame header = msg.Pop())
                        {
                            if (header.ToString().Equals(Majordomolib.MdpCommon.MDPC_CLIENT))
                                broker.ClientMsg(sender, msg);
                            else if (header.ToString().Equals(Majordomolib.MdpCommon.MDPW_WORKER))
                                broker.WorkerMsg(sender, msg);
                            else
                            {
                                eventLog1.WriteEntry("error in the message received");
                                msg.Dispose();
                            }
                        }
                    }
                    else
                    {
                        if (Equals(error, ZError.ETERM))
                        {
                            eventLog1.WriteEntry("error ETERM");
                            break; // Interrupted
                        }
                        if (!Equals(error, ZError.EAGAIN))
                        {
                            eventLog1.WriteEntry("Error is not EAGAIN");
                            throw new ZException(error);
                        }
                    }
                    // Disconnect and delete any expired workers
                    // Send heartbeats to idle workes if needed
                    if (DateTime.UtcNow > broker.HeartbeatAt)
                    {
                        broker.Purge();

                        foreach (var waitingworker in broker.Waiting)
                        {
                            waitingworker.Send(Majordomolib.MdpCommon.MdpwCmd.HEARTBEAT.ToHexString(), null, null);
                        }
                        broker.HeartbeatAt = DateTime.UtcNow + Majordomolib.MdpCommon.HEARTBEAT_INTERVAL;
                    }
                } // end while
                eventLog1.WriteEntry("After Broker function.");
            }
        }
    }
}
