**Majordomo Broker Windows Service**
---

* The setup binaries are available in the [Download section](https://bitbucket.org/hojjat12000/brokerservice/downloads/)
* After installation make sure that the srevice is running (by checking the service tab in Taskmanager)
* You can either run the service from the Services window or just restart the system
* make sure that the port 5456 is open in your firewall so that the service work over network too.
